﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

    public GameObject radioTower;
    public GameObject[] obstacles;
    public GameObject[] aesthetics;

    private List<GameObject> children = new List<GameObject>();

    // percentage sparsity
    // eg. 0.3 means 30% chance any area will not have an item spawned in it
    public float sparsity;
    // 0 means more scoring, 1 means more obstacles
    public float scoreObstacleRatio;

    public float startX;
    public float startY;

    public float endX;
    public float endY;

    void Awake()
    {
        Generate();
    }

	// Use this for initialization
	public void Generate () {

        float currentX = startX;
        float currentY = startY;
        bool done = false;

        foreach(GameObject obj in children)
        {
           Destroy(obj);
        }

		while(!done)
        {
            if(currentY == 0 && (currentX == startX || currentX == endX))
            {
                continue;
            }

            if(Random.Range(0f, 1f) > sparsity)
            {
                if(Random.Range(0f, 1f) > scoreObstacleRatio)
                {
                    GameObject tower = Instantiate(radioTower, this.gameObject.transform);
                    tower.transform.position = new Vector3(currentX + Random.Range(-0.5f, 0.5f), currentY + Random.Range(-0.5f, 0.5f), 20f);
                    children.Add(tower);
                } else
                {
                    int obstacle = Random.Range(0, obstacles.Length);
                    GameObject go = Instantiate(obstacles[obstacle], new Vector3(currentX + Random.Range(-0.5f, 0.5f), currentY + Random.Range(-0.5f, 0.5f), 20f), Quaternion.identity);
                    children.Add(go);
                }
            } else
            {
                int aesthetic = Random.Range(0, aesthetics.Length);
                GameObject go = Instantiate(aesthetics[aesthetic], new Vector3(currentX + Random.Range(-0.5f, 0.5f), currentY + Random.Range(-0.5f, 0.5f), 20f), Quaternion.identity);
                children.Add(go);
            }

            currentX += 1;
            if(currentX > endX)
            {
                currentX = startX;
                currentY += 1;
                if(currentY > endY)
                {
                    done = true;
                }
            }
        }
	}
}
