﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioControl : MonoBehaviour {

    public PlayerObject controller;
    public ParticleSystem onHit;

    public Color electro;
    public Color classical;

    public AudioSource[] bings;

    SpriteRenderer radioColor;

    private float particleIsAlive = 0f;

    void Start()
    {
        radioColor = GetComponent<SpriteRenderer>();
        onHit = GetComponent<ParticleSystem>();
        onHit.Stop();
    }

    void Update()
    {
        if(particleIsAlive > 0f)
        {
            particleIsAlive -= Time.deltaTime;
            if(particleIsAlive < 0f)
            {
                onHit.Stop();
            }
        }
    }

    public void reset()
    {
        controller = null;
        if (radioColor != null)
        {
            radioColor.color = Color.white;
        }
    }

    public void selectPlayer(PlayerObject p)
    {
        controller = p;
        if (controller.getPlayerNumber() == 1)
        {
            bings[0].pitch = Random.Range(0.8f, 1.2f);
            bings[0].Play();
            radioColor.color = electro;
            onHit.startColor = electro;
        }
        else if (controller.getPlayerNumber() == 2)
        {
            bings[0].pitch = Random.Range(0.8f, 1.2f);
            bings[1].Play();
            radioColor.color = classical;
            onHit.startColor = classical;
        }
        onHit.Play();
    }

    void OnCollisionEnter2D(Collision2D collider) { 
        if(collider.gameObject.GetComponent<VelocitySelector>() == null)
        {
            return;
        }
        selectPlayer(collider.gameObject.GetComponent<VelocitySelector>().owner);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.GetComponent<VelocitySelector>() == null)
        {
            return;
        }
        selectPlayer(collider.gameObject.GetComponent<VelocitySelector>().owner);
    }
}
