﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayController : MonoBehaviour {

    float timeElapsed = 0f;
    public SpriteRenderer howToPlay;
    public UnityEngine.UI.Text pressCtrl;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeElapsed += Time.deltaTime;

        if(timeElapsed < 3f)
        {
            Color logoColor = howToPlay.color;
            logoColor.a += 0.01f;
            howToPlay.color = logoColor;
        } else
        {
            Color txtColor = pressCtrl.color;
            txtColor.a += 0.01f;
            pressCtrl.color = txtColor;
            if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2"))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("TestScene");
            }
        }
	}
}
