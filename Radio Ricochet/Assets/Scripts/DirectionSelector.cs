﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionSelector : MonoBehaviour {

    public GameObject spawn;
    public GameObject target;

    public float speed;

    public PlayerObject player;
    public GameObject electro;
    public GameObject classical;

    public float velocity;
    public bool hasShot = false;
    SpriteRenderer sprender;

    // Use this for initialization
    void Start () {
		sprender = gameObject.GetComponent<SpriteRenderer>();
    }
	
    void Update() {
        if(Input.GetButtonDown("Fire" + player.getPlayerNumber().ToString()) && !hasShot)
        {
            hasShot = true;
        }
    }

    public void reenable() {
        hasShot = false;
        sprender.enabled = true;
    }

    public void fire(float lifetime)
    {
        GameObject selected = player.getPlayerNumber() == 1 ? electro : classical;
        GameObject newSignal = Instantiate(selected, spawn.transform.position, selected.transform.rotation);
        newSignal.transform.LookAt(target.transform.position);
        newSignal.transform.Rotate(Vector3.forward, 90f);
        newSignal.GetComponent<Rigidbody2D>().AddForce(newSignal.transform.forward * velocity);
        newSignal.GetComponent<VelocitySelector>().owner = player;
        newSignal.GetComponent<VelocitySelector>().lifetime = lifetime;
        hasShot = true;
        sprender.enabled = false;
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (!hasShot){
            float rotationDir;
            if(player.getPlayerNumber() == 1)
            {
                rotationDir = 1f;
            } else
            {
                rotationDir = -1f;
            }
            this.transform.RotateAround(spawn.transform.position, Vector3.forward, rotationDir * speed * Time.deltaTime);
        }
	}
}
