﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocitySelector : MonoBehaviour {
    // Use this for initialization
    public PlayerObject owner;
    public float lifetime;

    private float aliveTime;

	void Start () {

	}

	// Update is called once per frame
	void FixedUpdate () {
        Vector2 vel = GetComponent<Rigidbody2D>().velocity;
        this.transform.LookAt(this.transform.position + new Vector3(vel.x, vel.y, 0));
        this.transform.Rotate(Vector3.forward, 90f);

        aliveTime += Time.deltaTime;
        if(aliveTime > lifetime)
        {
            Destroy(this);
        }
    }
}
