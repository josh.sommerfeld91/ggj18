﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonLocomoter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Vector3 direction = Vector3.zero - this.transform.position;
        this.GetComponent<Rigidbody2D>().AddForce(direction * 5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
