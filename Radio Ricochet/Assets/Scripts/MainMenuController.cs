﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour {

    public SpriteRenderer GGJLogo;
    public SpriteRenderer MainMenuLogo;

    private float timeElapsed;
    public UnityEngine.UI.Text pressCtrl;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeElapsed += Time.deltaTime;

        if(timeElapsed < 2)
        {
            Color logoColor = GGJLogo.color;
            logoColor.a += 0.01f;
            GGJLogo.color = logoColor;
        } else if(timeElapsed > 3 && timeElapsed < 5)
        {
            Color logoColor = GGJLogo.color;
            logoColor.a -= 0.01f;
            GGJLogo.color = logoColor;
        } else if(timeElapsed > 5 && timeElapsed < 7)
        {
            Color logoColor = MainMenuLogo.color;
            logoColor.a += 0.01f;
            MainMenuLogo.color = logoColor;
        } else if(timeElapsed > 7)
        {
            Color txtColor = pressCtrl.color;
            txtColor.a += 0.01f;
            pressCtrl.color = txtColor;
            if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2"))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("HowToPlay");
            }
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                Debug.Log("Exitting Game.");
                Application.Quit();
            }
        }
		
	}
}
